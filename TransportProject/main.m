//
//  main.m
//  TransportProject
//
//  Created by Lera on 04.12.15.
//  Copyright © 2015 Valeriya.Mozgovaya. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <math.h>

//Types of transport
typedef enum {
    train = 1,
    truck = 2,
    bus= 3,
    bycicle = 4,
    tram =5,
    
} TypeTransport;

//Colors of transport
typedef enum{
    
    red = 1,
    green = 2,
    blue =3
    
} Color;

typedef struct {
    TypeTransport typetransport;
    Color color;
} Transport;

//Function for Transport creation
void createTransport();
//Function for getting random
int getRandomRange();
int main(int argc, const char * argv[]) {
    @autoreleasepool {
        createTransport();
    }
    return 0;
}

int getRandomInRange(int lowerBound, int upperBound){
    int randValue = lowerBound+arc4random_uniform(upperBound-lowerBound);
    return randValue;
}

void createTransport()
{
    Transport transport;
    
    for(int i=0; i<4; i++)
    {
        transport.typetransport = getRandomInRange(1,5);
        transport.color = getRandomInRange(1, 3);
        NSLog(@"your transport is %i type and %i color", transport.typetransport, transport.color);
    }
    
    
}
